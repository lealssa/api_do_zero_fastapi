from fastapi import FastAPI, APIRouter, HTTPException
from datetime import datetime
import pytz

router = APIRouter(prefix="/api/v1")

app = FastAPI(
    title="Minha Primeira API",
    description="API que retorna hora local e mundial",
    version="1.0.0",
    contact={
        "name": "Tiago",
        "email": "tiagoleal.ssa@gmail.com",
    }
)

@router.get("/hello")
async def get_hello_world():
    resp = { "resposta": "Hello World" }
    return resp

@router.get("/hora")
async def get_hora():
    resp = { "resposta": datetime.now() }
    return resp

@router.get("/hora/mundial")
async def get_hora_mundial(cidade: str):
    cidades_tz = { "Sao Paulo": "America/Sao_Paulo", "New York": "America/New_york" }
    
    if not cidade in cidades_tz.keys():
        raise HTTPException(status_code=404, detail=f"{cidade} não encontrada")
    
    tz = pytz.timezone(cidades_tz[cidade])
    
    resp = { "resposta": datetime.now(tz) }    
    return resp

app.include_router(router)